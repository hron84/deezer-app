const {app, BrowserWindow} = require('electron');
const path = require('path');

let mainWindow;

app.on('window-all-closed', function() {
  if (process.platform != 'darwin')
    app.quit();
});

let ppapi_flash_path;
let icon_name;

let USER_AGENT = 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36';

try {
  ppapi_flash_path = app.getPath('pepperFlashSystemPlugin');
} catch(err) {

  // Specify flash path.
  // On Windows, it might be /path/to/pepflashplayer.dll
  // On OS X, /path/to/PepperFlashPlayer.plugin
  // On Linux, /path/to/libpepflashplayer.so
  if(process.platform  == 'win32'){
    ppapi_flash_path = path.join(__dirname, 'plugins', 'pepflashplayer.dll');
    icon_name = path.join(__dirname, 'deezer.ico');
    app.commandLine.appendSwitch('ppapi-flash-version', '23.0.0.207');
  } else if (process.platform == 'linux') {
    ppapi_flash_path = path.join(__dirname, 'plugins','libpepflashplayer.so');
    icon_name = path.join(__dirname, 'deezer.png');
    app.commandLine.appendSwitch('ppapi-flash-version', '23.0.0.207');
  } else if (process.platform == 'darwin') {
    ppapi_flash_path = path.join(__dirname, 'plugins','PepperFlashPlayer.plugin');
    app.commandLine.appendSwitch('ppapi-flash-version', '23.0.0.207');
    icon_name = path.join(__dirname, 'deezer.png');
  }

}

app.commandLine.appendSwitch('ppapi-flash-path', ppapi_flash_path);

// Specify flash version, for example, v18.0.0.203

app.on('ready', function() {

  mainWindow = new BrowserWindow({
    'width': 800,
    'height': 600,
    'icon': icon_name,
    'webPreferences': {
      'plugins': true,
      'webSecurity': false,
      'allowDisplayingInsecureContent': true
    }
  });
  mainWindow.loadURL('https://www.deezer.com/', { userAgent: USER_AGENT});
  //mainWindow.loadURL('http://flashinfo.hron.me/');
  mainWindow.webContents.on('did-finish-load', function() { 
    // Quick fix for navigation issues when user login or relogin.
    this.executeJavaScript('if(document.location.href == "https://www.deezer.com/%2F") { document.body.innerHTML = "Loading, please wait..." ;  document.location.href = "https://www.deezer.com/"; }');
  });
});


// vim: ts=2 sw=2 et
